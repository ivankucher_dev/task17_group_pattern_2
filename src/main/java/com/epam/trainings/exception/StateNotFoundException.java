package com.epam.trainings.exception;

public class StateNotFoundException extends Exception {
    public StateNotFoundException(String message){
        super(message);
    }
}
