package com.epam.trainings.controller;

import com.epam.trainings.exception.StateNotFoundException;
import com.epam.trainings.model.User;
import com.epam.trainings.model.state.impl.*;
import com.epam.trainings.model.task.Task;
import com.epam.trainings.model.task.Tasks;
import com.epam.trainings.utils.TaskParserJson;
import com.epam.trainings.utils.TaskToViewAdapter;
import com.epam.trainings.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.epam.trainings.utils.PropertiesReader.getProperty;

public class ControllerImpl implements Controller {
  private static Logger log = LogManager.getLogger(ControllerImpl.class.getName());
  private View view;
  private Map<Integer, Class> statesPriority;
  private Map<String, List<Task>> tasksByState;
  private Tasks tasks;
  private User user;
  private Scanner sc;
  private TaskToViewAdapter taskToViewAdapter;

  public ControllerImpl(View view) {
    tasks = new Tasks();
    tasks.setTasks(new ArrayList<>(new TaskParserJson().read(getProperty("json_path"))));
    sc = new Scanner(System.in);
    initPrioritiesOfStates();
    fillTasksMap();
    taskToViewAdapter = new TaskToViewAdapter(tasks, statesPriority);
    this.view = view;
    this.view.setController(this);
  }

  public void createTask() {
    view.toShow("Enter number of task : ");
    int number = sc.nextInt();
    sc.nextLine();
    view.toShow("enter description : ");
    String desc = sc.nextLine();
    tasks.addTaskByFields(String.valueOf(number), desc);
    updateView();
  }

  public void chooseTask() {
    if (user == null) {
      view.toShow("You need to login user before choosing");
      view.updateView();
      return;
    }
    view.toShow("Enter number of task to choose : ");
    int number = sc.nextInt();
    if (tasks.getTaskByNumber(String.valueOf(number)).isPresent()) {
      user.chooseTask(tasks.getTaskByNumber(String.valueOf(number)).get());
    }
    updateView();
  }

  public void showKanbab() {
    taskToViewAdapter.updateTasks(tasks);
    view.toShow(taskToViewAdapter.outputKanbab());
    view.updateView();
  }

  public void loginUser() {
    view.toShow("Welcome to login form , please fill it : ");
    sc.nextLine();
    view.toShow("Enter username : ");
    String username = sc.nextLine();
    view.toShow("Enter password : ");
    int pass = sc.nextInt();
    this.user = new User(username, String.valueOf(pass));
    user.setTasks(tasks.getTasks());
    view.updateView();
  }

  public void moveToDone() {
    if (user == null) {
      view.toShow("You need to login user before moving");
      view.updateView();
      return;
    }
    view.toShow("Input number of task to move : ");
    int number = sc.nextInt();
    if (tasks.getTaskByNumber(String.valueOf(number)).isPresent()) {
      user.moveToDone(tasks.getTaskByNumber(String.valueOf(number)).get());
    }
    view.updateView();
  }

  public void moveToProgress() {
    if (user == null) {
      view.toShow("You need to login user before moving");
      view.updateView();
      return;
    }
    view.toShow("Input number of task to move : ");
    int number = sc.nextInt();
    if (tasks.getTaskByNumber(String.valueOf(number)).isPresent()) {
      user.moveToProgress(tasks.getTaskByNumber(String.valueOf(number)).get());
    }
    view.updateView();
  }

  public void moveToReview() {
    if (user == null) {
      view.toShow("You need to login user before moving");
      view.updateView();
      return;
    }
    view.toShow("Input number of task to move : ");
    int number = sc.nextInt();
    if (tasks.getTaskByNumber(String.valueOf(number)).isPresent()) {
      user.moveToReadyForReview(tasks.getTaskByNumber(String.valueOf(number)).get());
    }
    view.updateView();
  }

  public void moveToTest() {
    if (user == null) {
      view.toShow("You need to login user before moving");
      view.updateView();
      return;
    }
    view.toShow("Input number of task to move : ");
    int number = sc.nextInt();
    if (tasks.getTaskByNumber(String.valueOf(number)).isPresent()) {
      user.moveToInTest(tasks.getTaskByNumber(String.valueOf(number)).get());
    }
    view.updateView();
  }

  @Override
  public void updateView() {
    showKanbab();
  }

  private void fillTasksMap() {
    tasksByState = new HashMap<>();
    for (int i = 0; i < tasksByState.size(); i++) {
      try {
        Class state = getStateByPriority(i);
        tasksByState.put(getStateName(state), tasks.getTasksByState(getStateByPriority(i)));
      } catch (StateNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

  private void initPrioritiesOfStates() {
    statesPriority = new HashMap<>();
    statesPriority.put(1, ToDoState.class);
    statesPriority.put(2, ProgressState.class);
    statesPriority.put(3, ReadyForReviewState.class);
    statesPriority.put(4, InTestState.class);
    statesPriority.put(5, DoneState.class);
  }

  private Class getStateByPriority(int priority) throws StateNotFoundException {
    Optional<Class> stateClass = Optional.of(statesPriority.get(priority));
    if (stateClass.isPresent()) {
      return stateClass.get();
    } else {
      throw new StateNotFoundException("State not found");
    }
  }

  private String getStateName(Class clazz) {
    return clazz.getSimpleName();
  }
}
