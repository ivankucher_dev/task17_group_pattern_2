package com.epam.trainings.controller;

public interface Controller {
  void updateView();

  void moveToDone();

  void moveToProgress();

  void moveToReview();

  void moveToTest();

  void loginUser();

  void chooseTask();

  void createTask();

  void showKanbab();
}
