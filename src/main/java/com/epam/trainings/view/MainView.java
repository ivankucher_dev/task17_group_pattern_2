package com.epam.trainings.view;

import com.epam.trainings.controller.Controller;
import com.epam.trainings.model.Menu;

import java.util.Scanner;

public class MainView implements View {

  private Controller controller;
  private Menu menu;
  private Scanner sc;
  private boolean signedIn = false;

  public MainView() {
    sc = new Scanner(System.in);
  }

  @Override
  public void toShow(String toShow) {
    System.out.println(toShow);
  }

  @Override
  public void updateView() {
    showMenu();
  }

  @Override
  public void showMenu() {
    System.out.println(menu.showMenu());
    int index = sc.nextInt();
    menu.execute(index);
  }

  public void setController(Controller controller) {
    this.controller = controller;
    this.menu = new Menu(controller);
    controller.updateView();
  }
}
