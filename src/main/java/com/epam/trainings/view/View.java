package com.epam.trainings.view;

import com.epam.trainings.controller.Controller;

public interface View {
    void toShow(String toShow);
    void updateView();
    void showMenu();
    void setController(Controller controller);
}
