package com.epam.trainings.main;

import com.epam.trainings.controller.Controller;
import com.epam.trainings.controller.ControllerImpl;
import com.epam.trainings.model.task.Task;
import com.epam.trainings.view.MainView;
import com.epam.trainings.view.View;

public class Main {

  public static void main(String[] args) {
    View view = new MainView();
    Controller controller = new ControllerImpl(view);
  }
}
