package com.epam.trainings.utils;

import com.epam.trainings.model.state.impl.ToDoState;
import com.epam.trainings.model.task.Task;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class TaskParserJson {

  public List<Task> read(String path) {
    Gson gson = new Gson();
    JsonReader reader = null;
    try {
      reader = new JsonReader(new FileReader(path));
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
    if (reader != null) {
      List<Task> tasks = Arrays.asList(gson.fromJson(reader, Task[].class));
      tasks.forEach((task -> task.setState(new ToDoState())));
      return tasks;
    }
    throw new NoSuchElementException();
  }
}
