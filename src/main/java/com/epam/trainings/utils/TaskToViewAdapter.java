package com.epam.trainings.utils;

import com.epam.trainings.model.state.impl.*;
import com.epam.trainings.model.task.Task;
import com.epam.trainings.model.task.Tasks;
import de.vandermeer.asciitable.AsciiTable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class TaskToViewAdapter {
 private StringBuilder sb = new StringBuilder();
 private Map<String,List<Task>> tasksByStates;
   private Map<Integer,Class> tasksPriority;
 private Tasks tasks;

    public TaskToViewAdapter(Tasks tasks,Map<Integer,Class> tasksPriority) {
        tasksByStates = new HashMap<>();
        this.tasks = tasks;
        this.tasksPriority = tasksPriority;
        updateTasks(this.tasks);
    }

    public String outputKanbab() {

      AsciiTable table = new AsciiTable();
      table.addRow(" TO DO "," PROGRESS "," READY FOR REVIEW "," IN TEST "," DONE ");
      table.addRule();
      for(int i=0;i<tasks.getTasks().size();i++){
          table.addRow(removeByPriority(1),removeByPriority(2),removeByPriority(3),removeByPriority(4),removeByPriority(5));
          table.addRule();
      }
      return table.render();
  }

  private String removeByPriority(int priority){
        int size = tasksByStates.get(tasksPriority.get(priority).getSimpleName()).size();
        if(size>0){
           return tasksByStates.get(tasksPriority.get(priority).getSimpleName()).remove(size-1).toString();
        }
        else {
            return "";
        }
  }
  public void updateTasks(Tasks tasks){
        tasksByStates.put(ToDoState.class.getSimpleName(),tasks.getToDoTasks());
      tasksByStates.put(ProgressState.class.getSimpleName(),tasks.getInProgressTasks());
      tasksByStates.put(ReadyForReviewState.class.getSimpleName(),tasks.getReadyForReviewTasks());
      tasksByStates.put(InTestState.class.getSimpleName(),tasks.getInTestTasks());
      tasksByStates.put(DoneState.class.getSimpleName(),tasks.getDoneTasks());
  }
}
