package com.epam.trainings.model;

import com.epam.trainings.model.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class User {
  private static final Logger logger = LogManager.getLogger(User.class.getName());
  private String username;
  private String password;
  private List<Task> tasks;

  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setTasks(List<Task> availableTasks) {
    this.tasks = availableTasks;
  }

  public void chooseTask(Task task) {
    if (this.tasks.contains(task)) {
      if (task.getUser() == null) {
        task.setUser(this);
      }
    } else {
      logger.warn("Task is already chosen");
    }
  }

  public List<Task> getTasks() {
    return tasks;
  }

  private boolean containsTask(Task task) {
    return this.tasks.contains(task);
  }

  private List<Task> getUserTasks() {
    return tasks;
  }

  private Optional<Task> getUserTaskByNumber(int number) {
    for (Task task : tasks) {
      if (task.getNumber().equals(String.valueOf(number))) {
        return Optional.ofNullable(task);
      }
    }
    return Optional.ofNullable(null);
  }

  public void moveToProgress(Task task) {
    if (containsTask(task)) {
      if (task.containsUser()) {
        if (task.getUser().equals(this)) {
          task.addToInProgress();
        }
      }
      else{
        logger.warn("You are not signed for this task");
      }
    }
  }

  public void moveToReadyForReview(Task task) {
    if (containsTask(task)) {
      if (task.containsUser()) {
        if (task.getUser().equals(this)) {
          task.moveToReadyForView();
        }
      }else{
        logger.warn("You are not signed for this task");
      }
    }
  }

  public void moveToInTest(Task task) {
    if (containsTask(task)) {
      if (task.containsUser()) {
        if (task.getUser().equals(this)) {
          task.moveToInTest();
        }
      }
      else{
        logger.warn("You are not signed for this task");
      }
    }
  }

  public void moveToDone(Task task) {
    if (containsTask(task)) {
      if (task.containsUser()) {
        if (task.getUser().equals(this)) {
          task.moveToDone();
        }
      }
      else{
        logger.warn("You are not signed for this task");
      }
    }
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;
    User user = (User) object;
    return Objects.equals(username, user.username) && Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, password, tasks);
  }
}
