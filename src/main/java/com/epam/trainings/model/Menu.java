package com.epam.trainings.model;

import com.epam.trainings.controller.Controller;

import java.util.HashMap;
import java.util.Map;

public class Menu {
  private HashMap<Integer, Runnable> menu;
  private Map<Integer, String> stringMenu;
  private Controller controller;

  public Menu(Controller controller) {
    this.controller = controller;
    fillMenu();
    fillStringMenu();
  }

  public String showMenu() {
    StringBuilder sb = new StringBuilder();
    stringMenu.forEach((k, v) -> sb.append(k + " - " + v+"\n"));
    return sb.toString();
  }

  public void execute(int input){
      if(menu.get(input)!=null)
      menu.get(input).run();
  }

  private void fillMenu() {
    menu = new HashMap<>();
    menu.put(1, controller::showKanbab);
    menu.put(2, controller::loginUser);
    menu.put(3, controller::chooseTask);
    menu.put(4, controller::createTask);
    menu.put(5, controller::moveToProgress);
    menu.put(6, controller::moveToReview);
    menu.put(7, controller::moveToTest);
    menu.put(8, controller::moveToDone);
  }

  private void fillStringMenu() {
      stringMenu = new HashMap<>();
    stringMenu.put(1, "Show kanbab");
    stringMenu.put(2, "Login User");
    stringMenu.put(3, "Choose Task");
    stringMenu.put(4, "Create task");
    stringMenu.put(5, "Move to Progress");
    stringMenu.put(6, "Move to Review");
    stringMenu.put(7, "move to Test");
    stringMenu.put(8, "move to Done");
  }
}
