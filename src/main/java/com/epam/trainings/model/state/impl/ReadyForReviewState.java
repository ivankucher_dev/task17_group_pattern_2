package com.epam.trainings.model.state.impl;

import com.epam.trainings.model.state.State;
import com.epam.trainings.model.task.Task;

public class ReadyForReviewState implements State {

  @Override
  public void addToProgress(Task task) {
    task.setState(new ProgressState());
  }

  @Override
  public String toString() {
    return "Ready for review";
  }

  @Override
  public void addToInTest(Task task) {
    task.setState(new InTestState());
  }
}
