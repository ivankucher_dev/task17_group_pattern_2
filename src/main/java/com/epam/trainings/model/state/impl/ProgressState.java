package com.epam.trainings.model.state.impl;

import com.epam.trainings.model.state.State;
import com.epam.trainings.model.task.Task;

public class ProgressState implements State {

  @Override
  public String toString() {
    return "In progress";
  }

  @Override
  public void addToReadyForReview(Task task) {
    task.setState(new ReadyForReviewState());
  }
}
