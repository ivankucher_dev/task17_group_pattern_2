package com.epam.trainings.model.state.impl;

import com.epam.trainings.model.state.State;
import com.epam.trainings.model.task.Task;

public class ToDoState implements State {

  @Override
  public String toString() {
    return "To do";
  }

  @Override
  public void addToProgress(Task task) {
    task.setState(new ProgressState());
  }
}
