package com.epam.trainings.model.state.impl;

import com.epam.trainings.model.state.State;

public class DoneState implements State {

  @Override
  public String toString() {
    return "Done";
  }
}
