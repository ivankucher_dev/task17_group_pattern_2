package com.epam.trainings.model.state.impl;

import com.epam.trainings.model.state.State;
import com.epam.trainings.model.task.Task;

public class InTestState implements State {

  @Override
  public String toString() {
    return "In test";
  }

  @Override
  public void addToDone(Task task) {
    task.setState(new DoneState());
  }
}
