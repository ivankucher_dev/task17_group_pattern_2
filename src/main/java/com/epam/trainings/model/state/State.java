package com.epam.trainings.model.state;

import com.epam.trainings.model.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

  Logger logger = LogManager.getLogger(State.class.getName());

  default void addToProgress(Task task) {
    logger.info("addToProgress is not allowed");
  }

  default void addToReadyForReview(Task task) {
    logger.info("addToReadyForReview is not allowed");
  }

  default void addToInTest(Task task) {
    logger.info("addToInTest is not allowed");
  }

  default void addToDone(Task task) {
    logger.info("addToDone is not allowed");
  }
}
