package com.epam.trainings.model.task;

import com.epam.trainings.model.User;
import com.epam.trainings.model.state.State;
import com.epam.trainings.model.state.impl.ToDoState;

public class Task {
  private String number;
  private String description;
  private State state;
  private User user;

  public Task(String number, String description) {
    this.number = number;
    this.description = description;
    this.state = new ToDoState();
  }

  public String getNumber() {
    return number;
  }

  public String getDescription() {
    return description;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public State getState() {
    return state;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setState(State state) {
    this.state = state;
  }

  public void addToInProgress() {
    this.state.addToProgress(this);
  }

  public void moveToReadyForView() {
    this.state.addToReadyForReview(this);
  }

  public void moveToInTest() {
    this.state.addToInTest(this);
  }

  public void moveToDone() {
    this.state.addToDone(this);
  }

  public boolean containsUser() {
    return getUser() == null ? false : true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("----Task----\n");
    sb.append("Number = " + number);
    sb.append("\nDescription : \n" + description);
    if (getUser() != null) {
      sb.append("\nUser : " + getUser().getUsername());
    }
    sb.append("\nstate:" + state.getClass().getSimpleName());
    sb.append("\n---------\n");
    return sb.toString();
  }
}
