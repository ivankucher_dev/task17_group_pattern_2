package com.epam.trainings.model.task;

import com.epam.trainings.model.state.impl.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Tasks {
  private List<Task> tasks;
  public Tasks() {
    this.tasks = new ArrayList<>();
  }

  public List<Task> getToDoTasks() {
    return getTasksByState(ToDoState.class);
  }
  public List<Task> getInProgressTasks() {
    return getTasksByState(ProgressState.class);
  }
  public List<Task> getInTestTasks() {
    return getTasksByState(InTestState.class);
  }
  public List<Task> getDoneTasks() {
    return getTasksByState(DoneState.class);
  }
  public List<Task> getReadyForReviewTasks() {
    return getTasksByState(ReadyForReviewState.class);
  }

  public List<Task> getTasksByState(Class clazz) {
    return this.tasks.stream()
        .filter(t -> t.getState().getClass() == clazz)
        .collect(Collectors.toList());
  }

  public Optional<Task> getTaskByNumber(String number){
    for(Task task : tasks){
      if(task.getNumber().equals(number)){
        return Optional.ofNullable(task);
      }
    }
    return Optional.ofNullable(null);
  }

  public void setTasks(List<Task> tasks) {
    this.tasks = tasks;
  }

  public List<Task> getTasks() {
    return tasks;
  }

  public void addTaskByFields(String number, String description) {
    Task task = new Task(number, description);
    tasks.add(task);
  }

  public void addTask(Task task) {
    tasks.add(task);
  }
}
